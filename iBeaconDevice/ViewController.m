//
//  ViewController.m
//  iBeaconDevice
//
//  Created by Nguyenh on 12/11/14.
//  Copyright (c) 2014 Nguyenh. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "UIAlertView+Blocks.h"

#define UUID @"98e47e2e-26c8-46e5-acdc-5d3a207e9ec5"

@interface ViewController () <CBPeripheralManagerDelegate, CBCentralManagerDelegate>
{
    BOOL _on;
    NSString* _permission;
    int _frame;
    NSInteger _major;
    NSInteger _minor;
}
@property (nonatomic, retain) CLBeaconRegion* myBeaconRegion;
@property (strong, nonatomic) NSDictionary *myBeaconData;
@property (strong, nonatomic) CBPeripheralManager *peripheralManager;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _on = NO;
    _permission = nil;
    _major = 0;
    _minor = 1089;
    [_imageSignal setImage:[UIImage imageNamed:@"frame0.png"]];
    
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:UUID];
    self.myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:_major minor:_minor identifier:@"com.tedmate.com"];
    self.myBeaconData = [self.myBeaconRegion peripheralDataWithMeasuredPower:nil];
    self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil options:nil];
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(IBAction)startBeaconBroadcast:(UIButton*)sender
{
    _on = !_on;
    if (_on)
    {
        if (!_permission)
        {
            [_btnStart setTitle:@"Stop" forState:UIControlStateNormal];
            [self.peripheralManager startAdvertising:self.myBeaconData];
            _frame = 0;
            [_UUID setText:[NSString stringWithFormat:@"UUID: %@", UUID]];
            [_Major setText:[NSString stringWithFormat:@"Major: %ld", (long)_major]];
            [_Minor setText:[NSString stringWithFormat:@"Minor: %ld", (long)_minor]];
        }
    }
    else
    {
        [_btnStart setTitle:@"Start" forState:UIControlStateNormal];
        [self.peripheralManager stopAdvertising];
        [_UUID setText:@"Stop broadcast"];
        [_Major setText:@""];
        [_Minor setText:@""];
    }
    [self animationEffect];
}

-(void)peripheralManagerDidUpdateState:(CBPeripheralManager*)peripheral
{

        if (peripheral.state == CBPeripheralManagerStatePoweredOn)
        {
            _permission = nil;
        }
        else if (peripheral.state == CBPeripheralManagerStatePoweredOff)
        {
            _permission = @"Bluetooth currently off";
            [self.peripheralManager stopAdvertising];
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Bluetooth currently off, please choose Setting to turn it on" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Setting",nil];
            [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == 1)
                {
                    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    [[UIApplication sharedApplication] openURL:url];
                }
            }];
        }
        else if (peripheral.state == CBPeripheralManagerStateUnsupported)
        {
            _permission = @"";
            [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Your device doesn't support BLE" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
}

-(void)animationEffect
{
    if (!_on)
    {
        _frame = 0;
        [_imageSignal setImage:[UIImage imageNamed:@"frame0.png"]];
        return;
    }
    else
    {
        if (_frame >= 3)
            _frame = 0;
        [_imageSignal setImage:[UIImage imageNamed:[NSString stringWithFormat:@"frame%d.png", ++_frame]]];
        [self performSelector:@selector(animationEffect) withObject:nil afterDelay:1.f];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)settingPressed:(id)sender {
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Ignite" message:@"Change follow format: <major>;<minor>" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 0)
        {
            NSString* txt = [[alert textFieldAtIndex:0] text];
            NSArray* data = [txt componentsSeparatedByString:@";"];
            if (data.count == 2)
            {
                _major = [data[0] integerValue];
                _minor = [data[1] integerValue];
            }
            else
            {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Wrong format" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
        }
    }];
}

@end
