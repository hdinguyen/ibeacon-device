//
//  ViewController.h
//  iBeaconDevice
//
//  Created by Nguyenh on 12/11/14.
//  Copyright (c) 2014 Nguyenh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UIImageView *imageSignal;
@property (weak, nonatomic) IBOutlet UILabel *UUID;
@property (weak, nonatomic) IBOutlet UILabel *Major;
@property (weak, nonatomic) IBOutlet UILabel *Minor;

@end

